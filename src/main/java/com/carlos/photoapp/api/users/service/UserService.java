package com.carlos.photoapp.api.users.service;

import org.springframework.security.core.userdetails.UserDetailsService;

import com.carlos.photoapp.api.users.shared.UserDTO;

public interface UserService extends UserDetailsService{
	UserDTO createUser(UserDTO userDetails);
	UserDTO getUserDetailByEmail(String email);
	//UserDetails loadUserByUsername(String username);
}
