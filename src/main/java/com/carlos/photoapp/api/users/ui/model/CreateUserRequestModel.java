package com.carlos.photoapp.api.users.ui.model;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class CreateUserRequestModel {
	@NotNull(message="No puede ser nulo")
	@Size(min=2, message="Debe ser mayor o igual a dos caracteres")
	private String firstname;
	
	@NotNull(message="No puede ser nulo")
	@Size(min=2, message="Debe ser mayor o igual a dos caracteres")
	private String lastname;
	
	@NotNull(message="No puede ser nulo")
	@Email
	private String email;
	
	@NotNull(message="No puede ser nulo")
	@Size(min=8, max=16, message="Debe ser mayor o igual a 8 caracteres y menor que 16 caracteres")
	private String password;
	
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	
}
