package com.carlos.photoapp.api.users.security;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.core.env.Environment;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.carlos.photoapp.api.users.service.UserService;
import com.carlos.photoapp.api.users.shared.UserDTO;
import com.carlos.photoapp.api.users.ui.model.LoginRequestModel;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;;

public class AuthenticationFilter extends UsernamePasswordAuthenticationFilter{
	private UserService userService;
	private Environment environment;
	
	public AuthenticationFilter(Environment environment, UserService userService, AuthenticationManager authenticationManager) {
		this.environment = environment;
		this.userService = userService;
		super.setAuthenticationManager(authenticationManager);
	}
	
	@Override
	public Authentication attemptAuthentication(HttpServletRequest req, 
			HttpServletResponse res) throws AuthenticationException{
		LoginRequestModel creds;
		try {
			creds = new ObjectMapper()
					.readValue(
							req.getInputStream(), 
							LoginRequestModel.class
					);
			return getAuthenticationManager().authenticate(
					new UsernamePasswordAuthenticationToken(
							creds.getEmail(), 
							creds.getPassword(), 
							new ArrayList<>())
					);
			
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		
	}
	@Override
	protected void successfulAuthentication(HttpServletRequest req, 
											HttpServletResponse res, 
											FilterChain chain, 
											Authentication auth) throws IOException, ServletException{
		
		String userName = ((User) auth.getPrincipal()).getUsername();
		UserDTO user = userService.getUserDetailByEmail(userName);
		
		String token = Jwts.builder()
				.setSubject(user.getUserId())
				.setExpiration(new Date(System.currentTimeMillis() + Long.parseLong(environment.getProperty("token.expiration_time"))))
				.signWith(SignatureAlgorithm.HS512, environment.getProperty("token.secret"))
				.compact();
		res.addHeader("token", token);
		res.addHeader("userId", user.getUserId());
	}
}
